package chapter3;

public class Bai1_Congty {

	private Integer ID;
    private String Ten;
    private String Dien_thoai;
    private String Mail;
    private String Dia_chi;
    private Integer Tuoi_Toi_thieu;
    private Integer Tuoi_Toi_da;
    private Long Muc_Luong_Toi_thieu;
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public String getTen() {
		return Ten;
	}
	public void setTen(String ten) {
		Ten = ten;
	}
	public String getDien_thoai() {
		return Dien_thoai;
	}
	public void setDien_thoai(String dien_thoai) {
		Dien_thoai = dien_thoai;
	}
	public String getMail() {
		return Mail;
	}
	public void setMail(String mail) {
		Mail = mail;
	}
	public String getDia_chi() {
		return Dia_chi;
	}
	public void setDia_chi(String dia_chi) {
		Dia_chi = dia_chi;
	}
	public Integer getTuoi_Toi_thieu() {
		return Tuoi_Toi_thieu;
	}
	public void setTuoi_Toi_thieu(Integer tuoi_Toi_thieu) {
		Tuoi_Toi_thieu = tuoi_Toi_thieu;
	}
	public Integer getTuoi_Toi_da() {
		return Tuoi_Toi_da;
	}
	public void setTuoi_Toi_da(Integer tuoi_Toi_da) {
		Tuoi_Toi_da = tuoi_Toi_da;
	}
	public Long getMuc_Luong_Toi_thieu() {
		return Muc_Luong_Toi_thieu;
	}
	public void setMuc_Luong_Toi_thieu(Long muc_Luong_Toi_thieu) {
		Muc_Luong_Toi_thieu = muc_Luong_Toi_thieu;
	}
    
    
}
