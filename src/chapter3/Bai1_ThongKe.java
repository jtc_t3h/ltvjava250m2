package chapter3;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class Bai1_ThongKe {

	public static void main(String[] args) throws JsonSyntaxException, JsonIOException, FileNotFoundException {

		Gson gson = new Gson();
		Bai1_CongTyVaDonVi congtyVaDonVi = gson.fromJson(new FileReader("/Users/phaolo/QLCT_1.json"), Bai1_CongTyVaDonVi.class);
		
		System.out.println(congtyVaDonVi.getCONG_TY().size()); // 1
		System.out.println(congtyVaDonVi.getDON_VI().length); // 6
	}

}
