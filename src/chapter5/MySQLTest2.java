package chapter5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Thực hiện demo kết nối CSDL dùng Java: Try With Resource
 * @author phaolo
 *
 */
public class MySQLTest2 {

	public static void main(String[] args) {
		
		// B2. Tạo chuỗi kết nối
		String url = "jdbc:mysql://localhost:3306/phan_cong_nhan_vien_1_1_nam"; // Thay đổi theo từng DBMS
		String username = "root";
		String password = "";
		
		// a. select 
		String select = "select * from don_vi";
		
		// B3. Thiết lập kết nối
		try (Connection con = DriverManager.getConnection(url, username, password);
				Statement st = con.createStatement();
				ResultSet rs = st.executeQuery(select);
				) {
			// B6. Xử lý kết quả (nếu thực thi câu lệnh Select)
			while(rs.next()) {
				// côt id
				System.out.println(rs.getInt(1) + "\t" + rs.getString("Ten"));
			}
		} catch (SQLException e) {
			System.out.println("Connection fail!");
			e.printStackTrace();
		}
		System.out.println("Done");

	}

}
