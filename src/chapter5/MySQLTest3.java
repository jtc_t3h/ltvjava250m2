package chapter5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Thực hiện demo kết nối CSDL dùng Java: Try With Resource
 * @author phaolo
 *
 */
public class MySQLTest3 {

	public static void main(String[] args) {
		
		// B2. Tạo chuỗi kết nối
		String url = "jdbc:mysql://localhost:3306/phan_cong_nhan_vien_1_1_nam"; // Thay đổi theo từng DBMS
		String username = "root";
		String password = "";
		
		String insert = "insert into don_vi(ten) values (?)";
		
		// B3. Thiết lập kết nối
		try (Connection con = DriverManager.getConnection(url, username, password);
				PreparedStatement st = con.prepareStatement(insert);
				) {
			con.setAutoCommit(false);
			
			st.setString(1, "Đơn vị G");
			st.executeUpdate();
			
			st.setString(1, "Đơn vị H");
			st.executeUpdate();
			
			con.commit(); // Bắt đầu thay dữ liệu tới DB
			con.setAutoCommit(true);
		} catch (SQLException e) {
			System.out.println("Connection fail!");
			e.printStackTrace();
		}
		System.out.println("Done");

	}

}
