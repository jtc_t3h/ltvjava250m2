package chapter5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class MySQLTest1 {

	public static void main(String[] args) {
		
		
		// B2. Tạo chuỗi kết nối
		String url = "jdbc:mysql://localhost:3306/phan_cong_nhan_vien_1_1_nam"; // Thay đổi theo từng DBMS
		String username = "root";
		String password = "";
		
		// B3. Thiết lập kết nối
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			con = DriverManager.getConnection(url, username, password);
			System.out.println("Connection successful.");
			
			// B4. Tạo đối tượng quản lý thực thi truy vấn -> đối tượng Statemnet
			st = con.createStatement();
			
			// B5: Thực thi truy vấn -> chia 2 nhóm: 
			// b. insert, update, delete
			String insert = "insert into don_vi(ten) values ('Đơn vị F')";
			if (st.executeUpdate(insert) == 1) {
				System.out.println("Insert successfull.");
			} else {
				System.out.println("Insert fail.");
			}
			
			// a. select 
			String select = "select * from don_vi";
			rs = st.executeQuery(select);
			
			// B6. Xử lý kết quả (nếu thực thi câu lệnh Select)
			while(rs.next()) {
				// côt id
				System.out.println(rs.getInt(1) + "\t" + rs.getString("Ten"));
			}
		} catch (SQLException e) {
			System.out.println("Connection fail!");
			e.printStackTrace();
		} finally {
			// B7. Đóng kết nối
			try {
				if (rs != null) rs.close();
				if (st != null)	st.close();
				if (con != null) con.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
		
		System.out.println("Done");

	}

}
