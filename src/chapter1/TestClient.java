package chapter1;

import java.util.Arrays;
import java.util.List;

public class TestClient {

	public static void main(String[] args) {

//		// Hiện thực Function Interface
//		Operation op1 = (double a, double b) ->  a + b;
//		System.out.println(op1.operator(2, 3));
//		
//		Operation op2 = (a, b) -> a - b;
//		System.out.println(op2.operator(10, 7));
//		
//		Operation op3 = (a, b) -> a*b;
//		System.out.println(op3.operator(3, 7));
//		
//		Operation op4 = (a, b) -> a/b;
//		System.out.println(op4.operator(10, 2));
//		
//		// Tạo ra danh sách các số nguyên -> từ hàm asList của lơp java.util.Arrays
		List<Integer> listOfInt = Arrays.asList(1, 5, 6, 2, 4, 9, 7);
//		
//		// Duyệt các phần tử của danh sách
//		listOfInt.forEach(element -> System.out.println(element));
		 
//		// Sắp xếp danh sách các phần tử trong list --> Theo thứ tự từ nhỏ đến lớn
//		listOfInt.sort((e1, e2) -> e1 - e2);
//		listOfInt.forEach(element -> System.out.println(element));
		
		// Sắp xếp danh sách các phần tử trong list --> Theo thứ tự từ LỚN đến NHỎ
		listOfInt.sort((e1, e2) -> e2 - e1);
		listOfInt.forEach(element -> System.out.print(element + ", "));
	}

}
