package chapter1;

@FunctionalInterface // Nếu như mà định nhiều hơn 1 hàm thì compiler lỗi
public interface Operation {
	
	public double operator(double a, double b);

//	// Tác vụ công 2 số thực
//	public double add(double a, double b);
//	
//	// Tác vụ trừ 2 số thực
//	public double subtract(double a, double b);
//	
//	// Định nghĩa thêm ham multu và divide
	
	
}
