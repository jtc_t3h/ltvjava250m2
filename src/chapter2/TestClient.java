package chapter2;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class TestClient {

	public static void main(String[] args) {

		// C1: Tạo Stream dùng hàm Stream.of
		Stream<Integer> s1 = Stream.of(new Integer[] {1,2,3,4,5});
		Stream<String> s2 = Stream.of("hello", "world", "java", "module2");
		
		// C2: Tạo đối tượng Stream dùng Collection interface (List, Set) -> gọi hàm stream() hoặc parallelStream()
		List<Double> listOfDouble = Arrays.asList(1.1, 2.2, 3.3);
		Stream<Double> s3 = listOfDouble.stream();
		
		// C3: Tạo đối tượng Stream dùng Stream.generate()
		Stream<Integer> s4 = Stream.generate(() -> {
			Random rd = new Random();
			return rd.nextInt();
		});

//		// C4: dùng Arrays.stream
//		Stream<Character> s5 = Arrays.stream(new Character[] {'a','b','c'});
//		
//		// Duyệt các phần tử
//		s1.forEach(e -> System.out.println(e));
//
//		// Dùng toán tử map
//		s3.map(e -> e * e).forEach(e -> System.out.println(e));
//		
		// Dung ham peek
//		s2.peek(e -> {}).map((String e) -> e.concat(" t3h")).forEach(e -> System.out.println(e));
		
		// sort
//		s2.sorted().forEach(e -> System.out.println(e));
		s2.sorted((String e1, String e2) -> e2.compareTo(e1)).forEach(e -> System.out.println(e));
	}

}
